package io.github.hermasyp.loginexample;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    TextInputEditText txtUsername,txtPassword;
    Button btnLogin,btnAddAcc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtUsername = (TextInputEditText) findViewById(R.id.txtUsername);
        txtPassword = (TextInputEditText) findViewById(R.id.txtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnAddAcc = (Button) findViewById(R.id.btnAddAcc);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login(txtUsername.getText().toString(),txtPassword.getText().toString());
            }
        });
    }

    void login(String Username,String Password){
        if((Username.equals("TonyStark") ) && (Password.equals("Hermas090496"))){
            Intent i = new Intent(this,HomeActivity.class);
            startActivity(i);
        }else{
            Toast.makeText(this,"Username Or Password is incorect !",Toast.LENGTH_SHORT).show();
        }

    }
}
